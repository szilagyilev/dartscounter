//
//  ViewController.swift
//  DartsCounter
//
//  Created by Szilagyi, Levente on 2019. 02. 23..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var firstNameLabel : UILabel!
    @IBOutlet weak var secondNameLabel : UILabel!
    
    private let pointValues : [String] = ["20","19","18","17","16","15","Bull","Triple","Double","3B"]
    private var counterRows : [CounterRow] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for value in pointValues {
            counterRows.append(CounterRow(withTitle: value, withPlayerOneScore: 0, withPlayerTwoScore: 0))
        }
    }
    
    @IBAction func undo() {
        undoManager?.undo()
    }
    
    @IBAction func reset() {
        let dialogMessage = UIAlertController(title: "Reset", message: "Are you sure you want to reset?", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: {[unowned self] (action) -> Void in
            self.counterRows = []
            for value in self.pointValues {
                self.counterRows.append(CounterRow(withTitle: value, withPlayerOneScore: 0, withPlayerTwoScore: 0))
            }
            
            self.tableView.reloadData()
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
        }
        
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func changeNames() {
        let alertController = UIAlertController(title: "Enter Player Names", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: {
            [unowned self] alert -> Void in
            let fNameField = alertController.textFields![0] as UITextField
            let sNameField = alertController.textFields![1] as UITextField

            self.firstNameLabel.text = fNameField.text != "" ? fNameField.text : "Player One"
            self.secondNameLabel.text = sNameField.text != "" ? sNameField.text : "Player Two"
        }))
        
        alertController.addTextField(configurationHandler: {[unowned self] (textField) -> Void in
            textField.placeholder = "Player One's Name"
            if(self.firstNameLabel.text != "Player One") {
                textField.text = self.firstNameLabel.text
            }
            textField.textAlignment = .center
        })
        
        alertController.addTextField(configurationHandler: {[unowned self] (textField) -> Void in
            textField.placeholder = "Player Two's Name"
            if(self.secondNameLabel.text != "Player Two") {
                textField.text = self.secondNameLabel.text
            }
            textField.textAlignment = .center
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension ViewController : UITableViewDelegate {

}

extension ViewController : UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return counterRows.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight: CGFloat = 75.0
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            rowHeight = 100.0
        }
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CounterRowCell", for: indexPath) as! CounterTableViewCell
        
        cell.set(sourceCounter: counterRows[indexPath.row], counterManagerDelegate: self)

        return cell
    }
}

extension ViewController : CounterManagerDelegate {
    func addChangeToHistory(counterRow: CounterRow, isFirstPlayer: Bool, oldValue: Int) {
        
        undoManager?.registerUndo(withTarget: counterRow, handler: {[unowned self](targetSelf) in
            if(isFirstPlayer) {
                counterRow.playerOneValue = oldValue
                print("P1 value in row \(counterRow.title) saved as:\(counterRow.playerOneValue)")
            } else {
                counterRow.playerTwoValue = oldValue
                print("P2 value in row \(counterRow.title) saved as:\(counterRow.playerTwoValue)")
            }
            self.tableView.reloadData()
        })
    }
}
