//
//  CounterRow.swift
//  DartsCounter
//
//  Created by Szilagyi, Levente on 2019. 02. 23..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

class CounterRow {

    var title: String = "-"
    var playerOneValue: Int = 0
    var playerTwoValue: Int = 0

    init(withTitle: String, withPlayerOneScore: Int, withPlayerTwoScore: Int){
        title = withTitle
        playerOneValue = withPlayerOneScore
        playerTwoValue = withPlayerTwoScore
    }
}
