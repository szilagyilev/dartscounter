//
//  CounterManagerDelegate.swift
//  DartsCounter
//
//  Created by Szilagyi, Levente on 2019. 02. 23..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

protocol CounterManagerDelegate: AnyObject{
    func addChangeToHistory(counterRow: CounterRow, isFirstPlayer: Bool, oldValue: Int)
}
