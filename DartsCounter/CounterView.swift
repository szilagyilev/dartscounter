//
//  CounterView.swift
//  DartsCounter
//
//  Created by Szilagyi, Levente on 2019. 02. 23..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CounterView: UIView {

    @IBOutlet var contentView : UIView!
    @IBOutlet weak var countLabel : UILabel!
    @IBOutlet weak var countButton : UIButton!
    
    private static let countIcons : [String] = [" ","╱","╳","╳⃝"]
    
    var counterValue : Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CounterView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.backgroundColor = .white
    }
    
    func displayChar() {
        countLabel.text = CounterView.countIcons[counterValue]
    }
}
