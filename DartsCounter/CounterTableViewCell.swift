//
//  CounterTableViewCell.swift
//  DartsCounter
//
//  Created by Szilagyi, Levente on 2019. 02. 23..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class CounterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var playerOneCounterView: CounterView!
    @IBOutlet weak var playerTwoCounterView: CounterView!
    @IBOutlet weak var titleLabel: UILabel!
    
    weak var counterRow: CounterRow?

    weak var counterManager: CounterManagerDelegate?
    
    func set(sourceCounter: CounterRow?, counterManagerDelegate: CounterManagerDelegate?) {
        guard let sC = sourceCounter else {
            return
        }
        
        counterManager = counterManagerDelegate
        counterRow = sC
        
        titleLabel?.text = sC.title
        setValueOfCounter(counterView: playerOneCounterView, toValue: sC.playerOneValue)
        setValueOfCounter(counterView: playerTwoCounterView, toValue: sC.playerTwoValue)
        
        playerOneCounterView.countButton.addTarget(self, action: #selector(playerOneButtonTapped), for: .touchUpInside)
        playerTwoCounterView.countButton.addTarget(self, action: #selector(playerTwoButtonTapped), for: .touchUpInside)
        
        print("Row \(counterRow!.title) set.")
    }
    
    func increaseValueOfCounter(counterView: CounterView) {
        if(counterView.counterValue<3) {
            counterView.counterValue += 1
        }
        counterView.displayChar()
    }

    func isMaxValueReachedOfCounter(counterView: CounterView) -> Bool {
        return counterView.counterValue == 3
    }

    func setValueOfCounter(counterView: CounterView, toValue: Int) {
        counterView.counterValue = toValue
        counterView.displayChar()
    }
    
    @objc func playerOneButtonTapped() {
        if(!isMaxValueReachedOfCounter(counterView: playerOneCounterView)) {
            counterManager?.addChangeToHistory(counterRow: counterRow!, isFirstPlayer: true, oldValue: playerOneCounterView.counterValue)
            increaseValueOfCounter(counterView: playerOneCounterView)
            counterRow?.playerOneValue = playerOneCounterView.counterValue
            print("P1 value in row \(counterRow!.title) changed to:\(counterRow!.playerOneValue)")
        }
    }
    
    @objc func playerTwoButtonTapped() {
        if(!isMaxValueReachedOfCounter(counterView: playerTwoCounterView)) {
            counterManager?.addChangeToHistory(counterRow: counterRow!, isFirstPlayer: false, oldValue: playerTwoCounterView.counterValue)
            increaseValueOfCounter(counterView: playerTwoCounterView)
            counterRow?.playerTwoValue = playerTwoCounterView.counterValue
            print("P2 value in row \(counterRow!.title) changed to:\(counterRow!.playerTwoValue)")
        }
    }
}
